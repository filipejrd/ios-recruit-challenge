//
//  FavoritesDataProvider.swift
//  The Movie App
//
//  Created by Filipe Jordão on 02/02/18.
//  Copyright © 2018 Filipe Jordão. All rights reserved.
//
import RealmSwift

class FavoritesDataProvider {
    let realm: Realm
    
    init() throws {
        self.realm = try Realm()
    }
    
    func saveMovie(_ movie: MovieDBModel) throws{
        try realm.write {
            realm.add(movie, update: true)
        }
    }
    
    func getMovie(with id: Int) -> MovieDBModel? {
        let movie = realm.object(ofType: MovieDBModel.self, forPrimaryKey: id)
        return movie
    }
    
    func getAllMovies() -> [MovieDBModel] {
        let movies : [MovieDBModel] = realm.objects(MovieDBModel.self).map { (movie) -> MovieDBModel in
            return movie
        }
        
        return movies
    }
    
    func removeMovie(_ movie: MovieDBModel) throws{
        try realm.write {
            self.realm.delete(movie)
        }
    }
}
