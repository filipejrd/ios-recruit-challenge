//
//  RequestError.swift
//  The Movie App
//
//  Created by Filipe Jordão on 01/02/18.
//  Copyright © 2018 Filipe Jordão. All rights reserved.
//

import Foundation

enum RequestError: Error {
    case noData
    case responseWrongFormat
    case notSuccessful
}
