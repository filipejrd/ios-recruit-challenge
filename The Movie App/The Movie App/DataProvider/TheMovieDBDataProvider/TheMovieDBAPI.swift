//
//  TheMovieDBAPI.swift
//  The Movie App
//
//  Created by Filipe Jordão on 01/02/18.
//  Copyright © 2018 Filipe Jordão. All rights reserved.
//

import Foundation
import Moya

enum TheMovieDBAPI {
    case showPopularMovies(page: Int)
    case getGenres
}

extension TheMovieDBAPI: TargetType {
    var baseURL: URL {
        return URL(string:"https://api.themoviedb.org/3")!
    }
    
    var path: String {
        switch self {
        case .getGenres:
            return "/genre/movie/list"
       
        case .showPopularMovies(page: _):
            return "/movie/popular"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getGenres, .showPopularMovies(page: _):
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        var params: [String : Any] = ["api_key":"1dbb7d290ce2cb88ef8c311f67afd994"]
        
        switch self {
        case .getGenres:
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
            
        case .showPopularMovies(page: let page):
            params["page"] = page
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        }
    }
    
    var headers: [String : String]? {
        return ["Content-type": "application/json"]
    }
}
