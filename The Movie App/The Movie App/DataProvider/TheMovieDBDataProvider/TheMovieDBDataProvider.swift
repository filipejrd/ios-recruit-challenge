//
//  TheMovieDBDataProvider.swift
//  The Movie App
//
//  Created by Filipe Jordão on 01/02/18.
//  Copyright © 2018 Filipe Jordão. All rights reserved.
//

import Foundation
import Moya

class TheMovieDBDataProvider {
    let provider = MoyaProvider<TheMovieDBAPI>()
    
    func getGenres(completion: @escaping (([Int:String]?,Error?)->Void)) {
        self.provider.request(.getGenres) { (result) in
            if let error = result.error {
                completion(nil,error)
                return
            }
            
            guard let response = result.value else {
                completion(nil, RequestError.noData)
                return
            }
            
            switch response.statusCode {
            case 200:
                guard let body = try! response.mapJSON() as? [String : Any] else {
                    completion(nil, RequestError.responseWrongFormat)
                    return
                }
                
                guard let genresMap = body["genres"] as? [[String:Any]] else {
                    completion(nil,RequestError.responseWrongFormat)
                    return
                }
                
                
                let genres = genresMap.reduce([:], { (current, genreEntry) -> [Int:String] in
                    guard let id = genreEntry["id"] as? Int,
                          let name = genreEntry["name"] as? String else {
                            return current
                    }
                    
                    var result = current
                    result[id] = name
                    return result
                })
                
                completion(genres, nil)
                return
                
            default:
                completion(nil,RequestError.notSuccessful)
                return
            }
        }
    }
    
    func getPopularMovies(page: Int, completion: @escaping (([Movie]?,Error?) -> Void)) {
        self.getGenres { (genres, error) in
            if error != nil {
                completion(nil,error)
                return
            }
            if let genres = genres {
                self.provider.request(.showPopularMovies(page: page)) { (result) in
                    if let error = result.error {
                        completion(nil,error)
                        return
                    }
                    
                    guard let response = result.value else {
                        completion(nil, RequestError.noData)
                        return
                    }
                    
                    switch response.statusCode {
                    case 200:
                        guard let body = try! response.mapJSON() as? [String : Any] else {
                            completion(nil, RequestError.responseWrongFormat)
                            return
                        }
                        
                        guard let moviesMap = body["results"] as? [[String:Any]] else {
                            completion(nil,RequestError.responseWrongFormat)
                            return
                        }
                        
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-dd-MM"
                        
                        let movies = moviesMap.flatMap({ (item) -> Movie? in
                            guard let id = item["id"] as? Int,
                                let title = item["title"] as? String,
                                let overview = item["overview"] as? String,
                                let genreIds = item["genre_ids"] as? [Int],
                                let imageString = item["poster_path"] as? String,
                                let imageURL = URL(string: "https://image.tmdb.org/t/p/w500\(imageString)"),
                                let releaseDateString = item["release_date"] as? String,
                                let releaseDate = formatter.date(from: releaseDateString)
                                
                                else {
                                    return nil
                            }
                            
                            
                            
                            let reducedGenres = genreIds.reduce("", { (current, id) -> String in
                                if let genre = genres[id] {
                                    if current.isEmpty {
                                        return genre
                                    } else {
                                        return "\(current), \(genre)"
                                    }
                                } else {
                                    return current
                                }
                            })
                            
                            let movie = Movie(title: title, overview: overview, genres: reducedGenres, imageURL: imageURL, releaseDate: releaseDate, id: id)
                            
                            return movie
                        })
                        
                        completion(movies,nil)
                        return
                        
                    default:
                        completion(nil,RequestError.notSuccessful)
                        return
                    }
                }
            }
        }
    }
}
