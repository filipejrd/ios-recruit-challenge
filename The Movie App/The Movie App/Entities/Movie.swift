//
//  Movie.swift
//  The Movie App
//
//  Created by Filipe Jordão on 02/02/18.
//  Copyright © 2018 Filipe Jordão. All rights reserved.
//

import Foundation
class Movie {
    let title: String
    let overview: String
    let genres: String
    let imageURL: URL
    let releaseDate: Date
    let id: Int
    var isFavorite: Bool = false
    
    init(title: String, overview: String, genres: String, imageURL: URL, releaseDate: Date, id: Int) {
        self.title = title
        self.overview = overview
        self.genres = genres
        self.imageURL = imageURL
        self.releaseDate = releaseDate
        self.id = id
    }
}

