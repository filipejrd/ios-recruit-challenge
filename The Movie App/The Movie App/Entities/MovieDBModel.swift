//
//  Movie.swift
//  The Movie App
//
//  Created by Filipe Jordão on 31/01/18.
//  Copyright © 2018 Filipe Jordão. All rights reserved.
//

import Foundation
import RealmSwift

class MovieDBModel: Object {
    @objc dynamic var title: String = ""
    @objc dynamic var overview: String = ""
    @objc dynamic var genres: String = ""
    @objc dynamic var imageURL: String = ""
    @objc dynamic var releaseDate: Date = Date()
    @objc dynamic var id: Int = 0
    
    
    override static func primaryKey() -> String? {
        return "id"
    }
    override static func ignoredProperties() -> [String] {
        return ["isFavorite"]
    }
}
