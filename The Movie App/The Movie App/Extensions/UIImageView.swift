//
//  UIImage.swift
//  The Movie App
//
//  Created by Filipe Jordão on 01/02/18.
//  Copyright © 2018 Filipe Jordão. All rights reserved.
//

import UIKit


let imageCache = NSCache<NSURL,UIImage>()

extension UIImageView {
    func downloadImage(from url: URL){
        if let image = imageCache.object(forKey: url as NSURL){
            self.image = image
            return
        }
        
        let session = URLSession.shared
        session.dataTask(with: url, completionHandler: { (data, response, error) -> Void in
            if let error = error {
                print(error)
                return
            }
            
            if let data = data {
                let queue =  DispatchQueue.main
                
                queue.async(execute: { () -> Void in
                    if let image = UIImage(data: data) {
                        imageCache.setObject(image, forKey: url as NSURL)
                        self.image = image
                    }
                })
            }
        }).resume()
    }
}
