//
//  FavoritesListInteractor.swift
//  The Movie App
//
//  Created by Filipe Jordão on 02/02/18.
//  Copyright © 2018 Filipe Jordão. All rights reserved.
//

import Foundation

class FavoritesListInteractor: FavoritesListInteractorInputProtocol{
    weak var output: FavoritesListInteractorOutputProtocol!
    
    let favoriteProvider: FavoritesDataProvider?
    
    init() {
        self.favoriteProvider = try? FavoritesDataProvider()
    }
    
    func requestFavoriteMovies() {
        if let movies = self.favoriteProvider?.getAllMovies().flatMap({ (dbModel) -> Movie? in
            if let imageURL = URL(string: dbModel.imageURL) {
                
                let movie = Movie(title: dbModel.title, overview: dbModel.overview, genres: dbModel.genres, imageURL: imageURL, releaseDate: dbModel.releaseDate, id: dbModel.id)
                
                movie.isFavorite = true
                
                return movie
            }
            return nil
        }) {
            self.output.didReceiveMovies(movies)
        }
    }
}
