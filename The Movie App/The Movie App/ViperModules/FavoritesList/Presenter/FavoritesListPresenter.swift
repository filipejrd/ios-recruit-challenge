//
//  FavoritesListPresenter.swift
//  The Movie App
//
//  Created by Filipe Jordão on 02/02/18.
//  Copyright © 2018 Filipe Jordão. All rights reserved.
//

import Foundation

class FavoritesListPresenter: FavoritesListPresenterProtocol {
    var interactor: FavoritesListInteractorInputProtocol!
    weak var view: FavoritesListViewProtocol!
    var router: FavoritesListRouterProtocol!
    
    private var movies = [Movie]()
    
    func viewWillAppear() {
        self.interactor.requestFavoriteMovies()
    }
    
    func movieForIndex(_ index: Int) -> Movie {
        return self.movies[index]
    }
    
    func numberOfMovies() -> Int {
        return self.movies.count
    }
    func didSelectMovie(with index: Int) {
        self.router.toNextModule(movie: self.movies[index])
    }
}

extension FavoritesListPresenter: FavoritesListInteractorOutputProtocol {
    func didReceiveMovies(_ movies: [Movie]) {
        self.movies = movies
        self.view.reloadData()
    }
}
