//
//  FavoritesListContracts.swift
//  The Movie App
//
//  Created by Filipe Jordão on 02/02/18.
//  Copyright © 2018 Filipe Jordão. All rights reserved.
//

import Foundation

protocol FavoritesListViewProtocol: class {
    var presenter: FavoritesListPresenterProtocol! { get set }
    func reloadData()
}

protocol FavoritesListPresenterProtocol {
    weak var view: FavoritesListViewProtocol! { get set }
    var interactor: FavoritesListInteractorInputProtocol! { get set }
    var router: FavoritesListRouterProtocol! { get set }
    
    func viewWillAppear()
    func movieForIndex(_ index: Int) -> Movie
    func numberOfMovies() -> Int
    func didSelectMovie(with index: Int)
}

protocol FavoritesListInteractorInputProtocol {
    weak var output: FavoritesListInteractorOutputProtocol! { get set }
    func requestFavoriteMovies()
}

protocol FavoritesListInteractorOutputProtocol: class {
    func didReceiveMovies(_ movies: [Movie])
}

protocol FavoritesListRouterProtocol {
    static func setupModule() -> FavoritesListViewProtocol
    func toNextModule(movie: Movie) 
}
