//
//  FavoritesListRouter.swift
//  The Movie App
//
//  Created by Filipe Jordão on 02/02/18.
//  Copyright © 2018 Filipe Jordão. All rights reserved.
//

import Foundation
import UIKit

class FavoritesListRouter: FavoritesListRouterProtocol {
    private var vc: UIViewController?
    
    static func setupModule() -> FavoritesListViewProtocol {
        let view = FavoritesListView()
        let presenter = FavoritesListPresenter()
        let interactor = FavoritesListInteractor()
        let router = FavoritesListRouter()
        
        view.presenter = presenter
        
        presenter.router = router
        presenter.view = view
        presenter.interactor = interactor
        
        interactor.output = presenter
        
        router.vc = view
        
        return view
    }
    func toNextModule(movie: Movie) {
        if let detailsVc = MovieDetailsRouter.setupModule(for: movie) as? UIViewController {
            self.vc?.navigationController?.pushViewController(detailsVc, animated: true)
        }
    }
}
