//
//  FavoritesListView.swift
//  The Movie App
//
//  Created by Filipe Jordão on 02/02/18.
//  Copyright © 2018 Filipe Jordão. All rights reserved.
//

import UIKit

class FavoritesListView: UIViewController {
    var presenter: FavoritesListPresenterProtocol!
    @IBOutlet weak var favoritesList: UITableView!
    
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.setTitleAndTabBarItem()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setTitleAndTabBarItem()
    }
    
    private func setTitleAndTabBarItem() {
        self.title = "Favorites"
        self.tabBarItem = UITabBarItem(title: "Favorites", image: #imageLiteral(resourceName: "favorite_empty_icon"), tag: 1)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerNib()
        self.favoritesList.separatorColor = UIColor.white
        self.favoritesList.dataSource = self
        self.favoritesList.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.presenter.viewWillAppear()
    }
    
    private func registerNib(){
        let nib = UINib(nibName: "FavoritesListCell", bundle: nil)
        self.favoritesList.register(nib, forCellReuseIdentifier: "cell")
    }
}

extension FavoritesListView: FavoritesListViewProtocol {
    func reloadData() {
        self.favoritesList.reloadData()
    }
}

extension FavoritesListView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? FavoritesListCell {
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy"
            
            let movie = self.presenter.movieForIndex(indexPath.row)
            cell.poster.downloadImage(from: movie.imageURL)
            cell.title.text = movie.title
            cell.overview.text = movie.overview
            cell.year.text = formatter.string(from: movie.releaseDate)
            
            return cell
        }
        
        return UITableViewCell()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.numberOfMovies()
    }
}

extension FavoritesListView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.presenter.didSelectMovie(with: indexPath.row)
    }
}
