//
//  MovieDetailsInteractor.swift
//  The Movie App
//
//  Created by Filipe Jordão on 02/02/18.
//  Copyright © 2018 Filipe Jordão. All rights reserved.
//

import Foundation

class MovieDetailsInteractor: MovieDetailsInteractorInputProtocol {
    weak var output: MovieDetailsInteractorOutputProtocol!
    let favoriteProvider: FavoritesDataProvider?
    
    init() {
        self.favoriteProvider = try? FavoritesDataProvider()
    }
    
    func addMovieAsFavorite(_ movie: Movie){
        do {
            let dbModel = MovieDBModel()
            dbModel.id = movie.id
            dbModel.title = movie.title
            dbModel.overview = movie.overview
            dbModel.imageURL = movie.imageURL.absoluteString
            dbModel.releaseDate = movie.releaseDate
            dbModel.genres = movie.genres
            
            try self.favoriteProvider?.saveMovie(dbModel)
        } catch {
            self.output.requestFailed()
        }
    }
    
    func removeMovie(_ movie: Movie) {
        do {
            if let movieToRemove = self.favoriteProvider?.getMovie(with: movie.id) {
                try self.favoriteProvider?.removeMovie( movieToRemove)
            }
        } catch {
            self.output.requestFailed()
        }
    }
}
