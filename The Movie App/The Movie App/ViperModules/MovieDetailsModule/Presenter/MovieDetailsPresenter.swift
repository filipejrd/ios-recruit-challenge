//
//  MovieDetailsPresenter.swift
//  The Movie App
//
//  Created by Filipe Jordão on 02/02/18.
//  Copyright © 2018 Filipe Jordão. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class MovieDetailsPresenter: MovieDetailsPresenterProtocol {
    weak var view: MovieDetailsViewProtocol!
    var interactor: MovieDetailsInteractorInputProtocol!
    var router: MovieDetailsRouterProtocol!
    
    private var movie: Movie
    
    init(movie: Movie) {
        self.movie = movie
    }
    
    func viewDidLoad() {
        self.view.showMovieInfo(movie)
    }
    
    func didPressFavoriteButton() {
        if movie.isFavorite {
            movie.isFavorite = false
            self.interactor.removeMovie(movie)
            
        }else {
            movie.isFavorite = true
            self.interactor.addMovieAsFavorite(movie)
        }
        
        self.view.showMovieInfo(movie)

    }
}

extension MovieDetailsPresenter: MovieDetailsInteractorOutputProtocol {
    func requestFailed() {
        //TODO
    }
}

