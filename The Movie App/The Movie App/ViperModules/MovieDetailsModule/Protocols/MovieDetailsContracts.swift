//
//  MovieDetailsContracts.swift
//  The Movie App
//
//  Created by Filipe Jordão on 02/02/18.
//  Copyright © 2018 Filipe Jordão. All rights reserved.
//

import Foundation

protocol MovieDetailsViewProtocol: class{
    var presenter: MovieDetailsPresenterProtocol! { get set }
    
    func showMovieInfo(_ movie: Movie)
}

protocol MovieDetailsPresenterProtocol {
    weak var view: MovieDetailsViewProtocol! { get set }
    var interactor: MovieDetailsInteractorInputProtocol! { get set }
    var router: MovieDetailsRouterProtocol! { get set }
    
    func viewDidLoad()
    func didPressFavoriteButton()
}

protocol MovieDetailsRouterProtocol {
    static func setupModule(for movie: Movie) -> MovieDetailsViewProtocol
}

protocol MovieDetailsInteractorInputProtocol {
    weak var output: MovieDetailsInteractorOutputProtocol! { get set }
    func addMovieAsFavorite(_ movie: Movie)
    func removeMovie(_ movie: Movie)
}

protocol MovieDetailsInteractorOutputProtocol: class {
    func requestFailed()
}
