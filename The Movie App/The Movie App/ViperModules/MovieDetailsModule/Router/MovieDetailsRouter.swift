//
//  MovieDetailsRouter.swift
//  The Movie App
//
//  Created by Filipe Jordão on 02/02/18.
//  Copyright © 2018 Filipe Jordão. All rights reserved.
//

import Foundation


class MovieDetailsRouter: MovieDetailsRouterProtocol {
    static func setupModule(for movie: Movie) -> MovieDetailsViewProtocol {
        let view = MovieDetailsView()
        let presenter = MovieDetailsPresenter(movie: movie)
        let interactor = MovieDetailsInteractor()
        let router = MovieDetailsRouter()
        
        view.presenter = presenter
        
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        
        interactor.output = presenter
        
        return view
    }
}
