//
//  MovieDetailsView.swift
//  The Movie App
//
//  Created by Filipe Jordão on 02/02/18.
//  Copyright © 2018 Filipe Jordão. All rights reserved.
//

import UIKit

class MovieDetailsView: UIViewController {
    var presenter: MovieDetailsPresenterProtocol!
    
    @IBOutlet weak var moviePoster: UIImageView!
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var movieGenre: UILabel!
    @IBOutlet weak var movieYear: UILabel!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var movieOverview: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.hidesBottomBarWhenPushed = true
    }
    
    @IBAction func didPressFavoriteButton(_ sender: UIButton) {
        self.presenter.didPressFavoriteButton()
    }
}

extension MovieDetailsView: MovieDetailsViewProtocol {
    func showMovieInfo(_ movie: Movie) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy"
        
        self.title = movie.title
        self.movieTitle.text = movie.title
        self.movieOverview.text = movie.overview
        self.movieYear.text = formatter.string(from: movie.releaseDate)
        self.favoriteButton.setBackgroundImage(movie.isFavorite ? #imageLiteral(resourceName: "favorite_full_icon") : #imageLiteral(resourceName: "favorite_empty_icon") , for: UIControlState.normal)
        self.movieGenre.text = movie.genres
        self.moviePoster.downloadImage(from: movie.imageURL)
    }
}
