//
//  MovieGridInteractor.swift
//  The Movie App
//
//  Created by Filipe Jordão on 01/02/18.
//  Copyright © 2018 Filipe Jordão. All rights reserved.
//

import Foundation

class MovieGridInteractor: MovieGridInteractorInputProtocol {
    weak var output: MovieGridInteractorOutputProtocol!
    
    
    let dataProvider = TheMovieDBDataProvider()
    let favoritesProvider = try? FavoritesDataProvider()
    
    func requestPopularMovies(page: Int) {
        self.dataProvider.getPopularMovies(page: page) { (movies, error) in
            if let error = error {
                self.output.requestFailed(with: error)
            } else {
                if let movies = movies {
                    let processedMovies = movies.map({ (movie) -> Movie in
                        if let _ = self.favoritesProvider?.getMovie(with: movie.id) {
                            movie.isFavorite = true
                        }
                        return movie
                    })
                    self.output.didReceive(movies: processedMovies)
                }
            }
        }
    }
}
