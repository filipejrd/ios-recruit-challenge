//
//  MovieGridPresenter.swift
//  The Movie App
//
//  Created by Filipe Jordão on 31/01/18.
//  Copyright © 2018 Filipe Jordão. All rights reserved.
//

import Foundation

class MovieGridPresenter: MovieGridPresenterProtocol{
    weak var view: MovieGridViewProtocol!
    var interactor: MovieGridInteractorInputProtocol!
    var router: MovieGridRouterProtocol!
    
    
    private var movies: [Movie] = []
    private var filteredMovies: [Movie] = []
    private var currentPage = 1
    private var isWaitingForResponse = false
    
    func viewDidLoad() {
        self.view.reloadData()
        self.view.showLoading()
        self.isWaitingForResponse = true
        self.interactor.requestPopularMovies(page: currentPage)
    }
    
    func viewCurrentElement(index: Int) {
        if index >= movies.count - 1 && !self.isWaitingForResponse {
            self.isWaitingForResponse = true
            self.interactor.requestPopularMovies(page: currentPage)
        }
    }
    func getNumberOfItems() -> Int {
        return self.filteredMovies.count
    }
    
    func getMovie(for index: Int) -> Movie {
        return self.filteredMovies[index]
    }
    
    func didSelectCell(with index: Int) {
        let movie = self.filteredMovies[index]
        self.router.toNextModule(movie: movie)
    }
    
    func didSetFilter(_ filter: String?) {
        self.view.removeMessageView()
        if filter == nil || filter!.isEmpty {
            self.filteredMovies = movies
        } else {
            self.filteredMovies = self.movies.filter({ (movie) -> Bool in
                movie.title.contains(filter!)
            })
            
            if filteredMovies.isEmpty {
                self.view.showCouldntFindError(searchObject: filter!)
            }
        }
        
        self.view.reloadData()
    }
}

extension MovieGridPresenter: MovieGridInteractorOutputProtocol {
    func didReceive(movies: [Movie]) {
        self.view.removeMessageView()
        self.movies.append(contentsOf: movies)
        self.filteredMovies = self.movies
        self.isWaitingForResponse = false
        self.currentPage += 1
        self.view.reloadData()
    }
    
    func requestFailed(with error: Error) {
        self.isWaitingForResponse = false
        self.view.showGenericError()
    }
}
