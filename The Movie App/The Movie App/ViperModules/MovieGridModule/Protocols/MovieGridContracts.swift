//
//  MovieGridContracts.swift
//  The Movie App
//
//  Created by Filipe Jordão on 30/01/18.
//  Copyright © 2018 Filipe Jordão. All rights reserved.
//

import Foundation

protocol MovieGridViewProtocol: class {
    var presenter: MovieGridPresenterProtocol! { get set }
    func reloadData()
    func showGenericError()
    func removeMessageView()
    func showCouldntFindError(searchObject: String)
    func showLoading()
}
protocol MovieGridPresenterProtocol {
    weak var view: MovieGridViewProtocol! { get set }
    var interactor: MovieGridInteractorInputProtocol! { get set }
    var router: MovieGridRouterProtocol! { get set }
    
    func viewDidLoad()
    func viewCurrentElement(index: Int)
    func getNumberOfItems() -> Int
    func getMovie(for index: Int) -> Movie
    func didSelectCell(with index: Int)
    func didSetFilter(_ filter: String?)
}
protocol MovieGridInteractorInputProtocol{
    weak var output: MovieGridInteractorOutputProtocol! { get set }
    
    func requestPopularMovies(page: Int)
}
protocol MovieGridInteractorOutputProtocol: class {
    func didReceive(movies: [Movie])
    func requestFailed(with error: Error)
}
protocol MovieGridRouterProtocol {
    static func setupModule() -> MovieGridViewProtocol
    
    func toNextModule(movie: Movie)
}
