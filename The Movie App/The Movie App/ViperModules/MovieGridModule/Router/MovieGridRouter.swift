//
//  MovieGridRouter.swift
//  The Movie App
//
//  Created by Filipe Jordão on 30/01/18.
//  Copyright © 2018 Filipe Jordão. All rights reserved.
//

import Foundation
import UIKit

class MovieGridRouter: MovieGridRouterProtocol{
    private var vc: UIViewController!
    
    static func setupModule() -> MovieGridViewProtocol {
        let view = MovieGridView()
        let presenter = MovieGridPresenter()
        let interactor = MovieGridInteractor()
        let router = MovieGridRouter()
        
        router.vc = view
        
        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.output = presenter
        
        return view
    }
    
    func toNextModule(movie: Movie) {
        if let detailsVc = MovieDetailsRouter.setupModule(for: movie) as? UIViewController {
            self.vc.navigationController?.pushViewController(detailsVc, animated: true)

        }
    }
}
