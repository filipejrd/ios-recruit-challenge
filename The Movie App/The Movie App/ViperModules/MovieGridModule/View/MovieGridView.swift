//
//  MovieGridView.swift
//  The Movie App
//
//  Created by Filipe Jordão on 30/01/18.
//  Copyright © 2018 Filipe Jordão. All rights reserved.
//

import UIKit
import SnapKit

class MovieGridView: UIViewController {
    var presenter: MovieGridPresenterProtocol!
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var moviesGrid: UICollectionView!
    
    private var customView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Popular"
        self.tabBarItem = UITabBarItem(title: "Movies", image: #imageLiteral(resourceName: "list_icon"), tag: 0)
        self.moviesGrid.register(UINib(nibName: "MovieGridCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        self.moviesGrid.dataSource = self
        self.moviesGrid.delegate = self
        self.searchBar.delegate = self
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.presenter.viewDidLoad()
    }
}

extension MovieGridView: MovieGridViewProtocol {
    func reloadData() {
        self.moviesGrid.reloadData()
    }
    func showGenericError(){
        self.customView = UINib(nibName: "GenericErrorView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
        
        self.view.addSubview(self.customView)
        self.customView.snp.makeConstraints { (make) in
            make.width.equalTo(self.moviesGrid)
            make.height.equalTo(self.moviesGrid)
            make.center.equalTo(self.moviesGrid)
        }
    }
    
    func showLoading() {
        self.customView = UINib(nibName: "Loading", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
        
        self.view.addSubview(self.customView)
        self.customView.snp.makeConstraints { (make) in
            make.width.equalTo(self.moviesGrid)
            make.height.equalTo(self.moviesGrid)
            make.center.equalTo(self.moviesGrid)
        }
    }
    
    func showCouldntFindError(searchObject: String) {
        
        let view = UINib(nibName: "CouldntFindErrorView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! CouldntFindErrorView
        
        view.message.text = "Sua busca por \"\(searchObject)\" não resultou em nenhum resultado."
        
        self.customView = view
        self.view.addSubview(self.customView)
        self.customView.snp.makeConstraints { (make) in
            make.width.equalTo(self.moviesGrid)
            make.height.equalTo(self.moviesGrid)
            make.center.equalTo(self.moviesGrid)
        }
    }
    
    func removeMessageView(){
        self.customView.removeFromSuperview()
    }
}

extension MovieGridView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.presenter.getNumberOfItems()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? MovieGridCell else {
            fatalError()
        }
        
        let movie = self.presenter.getMovie(for: indexPath.item)
        cell.title.text = movie.title
        cell.image.image = nil
        cell.favorite.image = movie.isFavorite ? #imageLiteral(resourceName: "favorite_full_icon") : #imageLiteral(resourceName: "favorite_gray_icon")
        
        cell.image.downloadImage(from: movie.imageURL)
        return cell
    }
}


extension MovieGridView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.presenter.viewCurrentElement(index: indexPath.item)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionUsableWidth = collectionView.frame.width - 60
        
        return CGSize(width: collectionUsableWidth/2, height: collectionUsableWidth/1.5)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.presenter.didSelectCell(with: indexPath.item)
    }
}

extension MovieGridView: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.presenter.didSetFilter(searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

