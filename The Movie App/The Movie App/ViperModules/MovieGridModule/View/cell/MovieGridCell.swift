//
//  MovieGridCell.swift
//  The Movie App
//
//  Created by Filipe Jordão on 30/01/18.
//  Copyright © 2018 Filipe Jordão. All rights reserved.
//

import UIKit

class MovieGridCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var favorite: UIImageView!
}
