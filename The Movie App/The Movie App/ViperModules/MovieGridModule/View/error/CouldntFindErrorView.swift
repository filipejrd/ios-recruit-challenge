//
//  CouldntFindErrorView.swift
//  The Movie App
//
//  Created by Filipe Jordão on 02/02/18.
//  Copyright © 2018 Filipe Jordão. All rights reserved.
//

import UIKit

class CouldntFindErrorView: UIView {
    @IBOutlet weak var message: UILabel!
}
