//
//  RoundedView.swift
//  The Movie App
//
//  Created by Filipe Jordão on 02/02/18.
//  Copyright © 2018 Filipe Jordão. All rights reserved.
//

import UIKit

@IBDesignable
class RoundedView: UIView {
    override var frame: CGRect{
        didSet{
            self.roundView()
        }
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        self.roundView()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.roundView()
    }
    
    func roundView(){
        self.layer.cornerRadius = self.frame.width/2
        self.layer.masksToBounds = true
    }
}
