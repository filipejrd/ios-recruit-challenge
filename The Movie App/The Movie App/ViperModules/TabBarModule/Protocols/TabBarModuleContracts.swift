//
//  TabBarModuleContracts.swift
//  The Movie App
//
//  Created by Filipe Jordão on 30/01/18.
//  Copyright © 2018 Filipe Jordão. All rights reserved.
//

import Foundation
import UIKit

protocol TabBarRouterProtocol {
    static func setupModule() -> TabBarViewProtocol
}

protocol TabBarViewProtocol {
    
}
