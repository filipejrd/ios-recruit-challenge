//
//  TabBarView.swift
//  The Movie App
//
//  Created by Filipe Jordão on 30/01/18.
//  Copyright © 2018 Filipe Jordão. All rights reserved.
//

import UIKit

class TabBarView: UITabBarController{
    var vcs : [Any] = [MovieGridRouter.setupModule(), FavoritesListRouter.setupModule()]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.barTintColor = #colorLiteral(red: 0.9690945745, green: 0.8084867597, blue: 0.3556014299, alpha: 1)
        self.tabBar.tintColor = #colorLiteral(red: 0.1757613122, green: 0.1862640679, blue: 0.2774662971, alpha: 1)
        self.tabBar.isTranslucent = false
       
        
        self.viewControllers = vcs.flatMap({ (view) -> UIViewController? in
            let navVc = UINavigationController(rootViewController: (view as! UIViewController))
            navVc.navigationBar.barTintColor = #colorLiteral(red: 0.9690945745, green: 0.8084867597, blue: 0.3556014299, alpha: 1)
            navVc.navigationBar.tintColor = #colorLiteral(red: 0.1757613122, green: 0.1862640679, blue: 0.2774662971, alpha: 1)
            navVc.navigationBar.isTranslucent = false
            navVc.navigationBar.setBackgroundImage(UIImage(), for: .default)
            navVc.navigationBar.shadowImage = UIImage()
            
            return navVc
        })
    }
}

extension TabBarView: TabBarViewProtocol {}
